## Keyboard Configuration

### Fix home and end keys
If you use an external keyboard and are used to how the `home` and `end` keys work under a PC or Linux, the following will allow similar behavior for your Mac.

*Note: This does not appear to work in Google Documents.*

#### For native Mac OS applications.

Create a new file in `~/Library/KeyBindings` called `DefaultKeyBinding.dict`

Insert the following commands into that file:

```
{
/* Remap Home / End to function as normal */
/* Home */
"\UF729" = "moveToBeginningOfLine:"; 
/* End */
"\UF72B" = "moveToEndOfLine:"; 
/* Shift + Home */
"$\UF729" = "moveToBeginningOfLineAndModifySelection:"; 
/* Shift + End */
"$\UF72B" = "moveToEndOfLineAndModifySelection:"; 
/* Ctrl + Home */
"^\UF729" = "moveToBeginningOfDocument:"; 
/* Ctrl + End */
"^\UF72B" = "moveToEndOfDocument:"; 
 /* Shift + Ctrl + Home */
"$^\UF729" = "moveToBeginningOfDocumentAndModifySelection:";
/* Shift + Ctrl + End */
"$^\UF72B" = "moveToEndOfDocumentAndModifySelection:"; 
}
```

A restart will be required before it works.

#### For Terminal.app

In `Terminal->Preferences`, select your profile.

In the `Keyboard` tab, add the following. Note, `\033` is the code for the `Esc` key. To type it into the shortcut, just press the `Esc` key.

| Key | String |
| --- | --- |
| `Home` | `\033OH` |
| `End` | `\033OF`|

Restart a terminal session.

#### For the iTerm2.app

Open profiles and select the default profile. Choose to edit the profile and select the Keys tab.

Add a new Key Mapping for `Home` and `End` by creating a new key binding and select `Send Hex Codes` from the menu.

The hex codes you want are as follows.

| Key | String |
| --- | --- |
| `Home` | `0x01` |
| `End` | `0x05`|


#### For Emacs

Add the following lines to your configuration.

```
(global-set-key [home] 'move-beginning-of-line)
(global-set-key [end] 'move-end-of-line)
```
