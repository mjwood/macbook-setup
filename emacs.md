## Emacs Configuration

### Installing Emacs version 27 or greater

You will need at least `emacs 27.0` or higher for to get colored fonts and emojis to work. That means that installing the standard `emacs` cask from homebrew (which is currently version 26.2) won't work. I'm installing `emacs 31.x` as I try to stay up to date as much as possible.

Thankfully, there's a homebrew tap that can build `emacs-head` - [daviderestiovo/homebrew-emacs-head](https://github.com/daviderestivo/homebrew-emacs-head)

If you want to run an existing version of Emacs along side this new version, you'll want to rename `/Applications/Emacs.app` --> `Emacs31.app`

```
brew tap daviderestivo/emacs-head
brew install emacs-head@31 --with-cocoa --with-imagemagick --with-modern-icon-elrumo1
ln -s /usr/local/opt/emacs-head/Emacs.app /Applications
```

Building from source allows you to choose options. Here, we're building with Cocoa support (which I prefer) as well as including ImageMagick (which will allow for better emoji resizing, etc...)

This installed Emacs 31.0 for me, which worked great.

### Utilize Better Fonts
The standard Emacs fonts aren't horrible, but [Hack](https://github.com/source-foundry/Hack) was made for source code editing and has a look I prefer. To install it within Emacs, first [download it](https://github.com/source-foundry/Hack#macos) and install it in Mac OS. To install a font in Mac OS, go open the zip file and click on each of the font files. They will bring up a dialog asking if you'd like to install each variant. 

Once installed, go ahead and tell Emacs to use them:

```
(when (eq system-type 'darwin)
 (set face attribute 'default nil
                     :height 150
                     :family "Hack"))
```

Font height in Emacs is 10 units per normal point. So `150` indicates 15pt font. Note, I have a really large monitor (3840x1600), so fonts this size aren't huge for me. Feel free to adjust as necessary.

_I should also note that for some reason, the `Fira` font seems to be missing from my clean MacOS install. Given this, it was necessary to install this font for my specific emacs config. I'm not sure if this is specific to my config, or needed by `emacs`_

### Emoji Support

#### Configure Emacs startup files
My `.emacs` file is shared between MacOS, Linux and sometimes Windows. Therefore, logic needs to be added to use the correct font when it comes to MacOS or Linux. I have not configured emoji support for Windows.

In my `.emacs` file, I have the following

```
;; Sort out emoji support for Mac and Linux
(message "Setting up fonts")
(if (<= 27 emacs-major-version)
    (if (eq system-type 'darwin)
	  ;; For NS/Cocoa
	  (progn
	    (message "Setting up emoji for Mac OS")
	    ;;(set-fontset-font "fontset-default" 'unicode "Apple Color Emoji" nil 'prepend))
	    (set-fontset-font t 'symbol (font-spec :family "Apple Color Emoji") nil 'prepend))
	;; For linux - Symbola can be installed with apt install ttf-ancient-fonts
	(progn (print "Setting emoji for Linux")
	       (set-fontset-font t 'symbol (font-spec :family "Symbola") nil 'prepend))
	))

```

This will allow display of emoji.
