## MacBook Configuration

### Hardware
I currently use a 16" 2021 MacBook Pro, with four Thunderbolt 3 ports

- Apple M1 Max Processor
- 64 GB RAM

I have it paired with a very large external display - a [Dell U3818DW](https://amzn.to/39QwjKX) with the following specs:

- 38" WQHD+ Curved Widescreen
- 3840 x 1600 Resolution
- USB Type-C connection

I can highly recommend this display with a couple of caveats with regards to the MacBook Pro compatibility:
- Connection via USB Type-C doesn't reliably work for video. I use a CalDigit dock and it works great. One single Thunderbolt connection to my MacBook and I get mouse, keyboard, webcam, sound interface, and full power charging.

My recommendation is to also install [Magnet](https://magnet.crowdcafe.com/) to help snap windows to configurations. Three full size browser windows can fit comfortable from left to right on this display!

I also have the following peripherals connected:
- [Massdrop CTRL mechanical Keyboard](https://drop.com/buy/drop-ctrl-mechanical-keyboard) - I got the version with theKaihua Box White (Clicky) switches. They aren't quite as clicky as MX Blues, but I really like them.
- [Logitech MX Master Wireless Mouse](https://amzn.to/36x4YLL) - I've had this for a while, and I really like it. It connects great over Bluetooth and can connect up to three devices. The only issue I have is that sometimes it reverses scrolling direction which I believe is a software issue. I'm not sure if the newer versions (there's the MX Master 2 & 3 out now) fix this. A quick power cycle of the mouse fixes it, so it's not too big a deal for a comfortable mouse.
- [AmazonBasics Gaming Computer Mouse pad](https://amzn.to/300jxFg) - It's about the size of a letter sized piece of paper, which is great. I've had mine for two years and it's held up well. It's also quite thin, which I really like.
- All of my audio is running through a USB-C [Motu M4](https://motu.com/en-us/products/m-series/m4/) sound interface. This provides me with dual inputs (XLR for my Shure SM58 Microphone), as well as dual outputs for both headphones and powered studio monitors. I did find that the SM58 needed a little boosting, so I paired it with a [Fethead](https://www.tritonaudio.com/fethead) which is powered by the 48V phantom power on the M4. 

### Software Setup

I perform the following customization to the standard Mac OS in order to suit my preferences.

- [Configure External Keyboard](keyboard.md) - I like to use an external keyboard, and I like it to work as expected with Linux (eg. home and end keys do normal home and end behavior)
- [Configure Emacs](emacs.md) - I like Emacs. Some don't. But for me, I've used it for close to 20 years, so I'm not planning on switching anytime soon.
- [Tweak macOS](osx.md) - There are some frustrating parts of macOS. I like to fix those where possible.